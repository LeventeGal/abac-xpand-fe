import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddPlanetComponent } from "./pages/add-planet/add-planet.component";
import { PlanetListComponent } from "./pages/planet-list/planet-list.component";
import { AddCrewComponent } from './pages/add-crew/add-crew.component';

const routes: Routes = [
  { path: "", component: PlanetListComponent },
  { path: "add-planet", component: AddPlanetComponent },
  { path: "add-crew", component: AddCrewComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
