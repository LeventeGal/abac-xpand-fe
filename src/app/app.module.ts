import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatToolbarModule
} from "@angular/material";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { FormComponent } from "./core/components/form/form.component";
import { XpandHttpClient } from "./core/XPandHttpClient";
import { AddCrewComponent } from "./pages/add-crew/add-crew.component";
import { AddPlanetComponent } from "./pages/add-planet/add-planet.component";
import { AssignCrewDialogComponent } from "./pages/planet-list/assign-crew-dialog/assign-crew-dialog.component";
import { PlanetListComponent } from "./pages/planet-list/planet-list.component";
import { UpdateStatusDialogComponent } from "./pages/planet-list/update-status-dialog/update-status-dialog.component";

@NgModule({
  declarations: [
    AppComponent,
    PlanetListComponent,
    AddPlanetComponent,
    AddCrewComponent,
    FormComponent,
    AssignCrewDialogComponent,
    UpdateStatusDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,

    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatMenuModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule,
    MatSelectModule
  ],
  providers: [XpandHttpClient],
  bootstrap: [AppComponent],
  entryComponents: [AssignCrewDialogComponent, UpdateStatusDialogComponent]
})
export class AppModule {}
