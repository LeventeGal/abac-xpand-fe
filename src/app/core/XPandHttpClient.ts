import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";

@Injectable()
export class XpandHttpClient {
  constructor(private httpClient: HttpClient) {}

  public get<T>(endpoint: string): Observable<T> {
    return this.httpClient.get<T>(environment.apiRootUrl + endpoint);
  }

  public post<D, R>(endpoint: string, data: D): Observable<R> {
    return this.httpClient.post<R>(environment.apiRootUrl + endpoint, data);
  }

  public put<D, R>(endpoint: string, data: D): Observable<R> {
    return this.httpClient.put<R>(environment.apiRootUrl + endpoint, data);
  }
}
