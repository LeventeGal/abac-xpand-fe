import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { FormGroup } from "@angular/forms";

@Component({
  selector: "xpand-form",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.scss"]
})
export class FormComponent implements OnInit {
  @Input()
  private formTitle;

  @Input()
  private formGroup: FormGroup;

  @Output()
  private submit = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  private onSubmit(event: Event){
    event.stopPropagation();
    
    this.submit.emit();
  }
}
