import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormGroup } from "@angular/forms";
import { XpandHttpClient } from "src/app/core/XPandHttpClient";
import { Router } from "@angular/router";

@Component({
  selector: "app-add-crew",
  templateUrl: "./add-crew.component.html",
  styleUrls: ["./add-crew.component.scss"]
})
export class AddCrewComponent implements OnInit {
  private control: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private httpClient: XpandHttpClient,
    private router: Router
  ) {}

  ngOnInit() {
    this.control = this.formBuilder.group({
      captain: null,
      robots: this.formBuilder.array([this.createRobotControl()])
    });
  }

  private handleAddButton() {
    this.getRobotsControl().push(this.createRobotControl());
  }

  private handleRemoveButton(index: number) {
    const robotsControl = this.getRobotsControl();

    robotsControl.removeAt(index);

    if (robotsControl.length == 0) {
      robotsControl.push(this.createRobotControl());
    }
  }

  private onSubmit() {
    this.httpClient.post("/crews", this.control.value).subscribe(
      () => this.router.navigate([""]),
      () => alert("Unexpected error. PLease try again later.")
    );
  }

  private createRobotControl(): FormGroup {
    return this.formBuilder.group({
      name: ""
    });
  }

  private getRobotsControl(): FormArray {
    return this.control.get("robots") as FormArray;
  }
}
