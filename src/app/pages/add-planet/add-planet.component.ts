import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { XpandHttpClient } from "src/app/core/XPandHttpClient";

@Component({
  selector: "app-add-planet",
  templateUrl: "./add-planet.component.html",
  styleUrls: ["./add-planet.component.scss"]
})
export class AddPlanetComponent implements OnInit {
  private control: FormGroup;

  constructor(private httpCLient: XpandHttpClient, private router: Router) {
    this.control = new FormGroup({
      name: new FormControl(null),
      imageUrl: new FormControl(null)
    });
  }

  ngOnInit() {}

  onSubmit() {
    this.httpCLient.post("/planets", this.control.value).subscribe(
      () => this.router.navigate([""]),
      () => alert("Unexpected error. Please try again later.")
    );
  }
}
