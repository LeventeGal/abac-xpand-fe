import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { MatDialogRef } from "@angular/material";

@Component({
  selector: "app-update-status-dialog",
  templateUrl: "./update-status-dialog.component.html",
  styleUrls: ["./update-status-dialog.component.scss"]
})
export class UpdateStatusDialogComponent implements OnInit {
  private control: FormGroup;

  constructor(
    private dialogRef: MatDialogRef<UpdateStatusDialogComponent>,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.control = this.formBuilder.group({
      status: "",
      description: ""
    });
  }

  private handleCancel(): void {
    this.dialogRef.close();
  }
}
