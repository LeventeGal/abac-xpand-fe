import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material";
import { BehaviorSubject, EMPTY, Observable, of } from "rxjs";
import { catchError, filter, switchMap } from "rxjs/operators";
import { XpandHttpClient } from "src/app/core/XPandHttpClient";
import { AssignCrewDialogComponent } from "./assign-crew-dialog/assign-crew-dialog.component";
import { UpdateStatusDialogComponent } from "./update-status-dialog/update-status-dialog.component";

@Component({
  selector: "app-planet-list",
  templateUrl: "./planet-list.component.html",
  styleUrls: ["./planet-list.component.scss"]
})
export class PlanetListComponent implements OnInit {
  private planets$: Observable<Planet[]>;

  private planetLoader$ = new BehaviorSubject(null);

  constructor(private httpClient: XpandHttpClient, public dialog: MatDialog) {}

  ngOnInit() {
    this.planets$ = this.planetLoader$.pipe(
      switchMap(() =>
        this.httpClient.get<Planet[]>("/planets").pipe(
          catchError(() => {
            alert("Unexpected error accured. PLease try again later.");

            return of([]);
          })
        )
      )
    );

    this.planetLoader$.next(null);
  }

  private handleAssignCrew(planetId: number) {
    const dialogRef = this.dialog.open(AssignCrewDialogComponent, {
      width: "50%"
    });

    dialogRef
      .afterClosed()
      .pipe(
        filter(result => result != null),
        switchMap(result => {
          return this.httpClient
            .post(`/planets/${planetId}/assign-crew`, result)
            .pipe(
              catchError(() => {
                alert("Unexpected error accured. Please try again later");

                return EMPTY;
              })
            );
        })
      )
      .subscribe(() => this.planetLoader$.next(null));
  }

  private handleUpdateStatus(planetId) {
    const dialogRef = this.dialog.open(UpdateStatusDialogComponent, {
      width: "50%"
    });

    dialogRef
      .afterClosed()
      .pipe(
        filter(result => result != null),
        switchMap(result => {
          console.warn(result);

          return this.httpClient
            .put(`/planets/${planetId}/status`, result)
            .pipe(
              catchError(() => {
                alert("Unexpected error accured. Please try again later");

                return EMPTY;
              })
            );
        })
      )
      .subscribe(() => this.planetLoader$.next(null));
  }
}

interface Planet {}
