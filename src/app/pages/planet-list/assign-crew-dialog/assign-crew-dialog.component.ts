import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { MatDialogRef } from "@angular/material";
import { of } from "rxjs";
import {
  catchError,
  debounceTime,
  filter,
  finalize,
  switchMap,
  tap
} from "rxjs/operators";
import { XpandHttpClient } from "src/app/core/XPandHttpClient";

@Component({
  selector: "app-assign-crew-dialog",
  templateUrl: "./assign-crew-dialog.component.html",
  styleUrls: ["./assign-crew-dialog.component.scss"]
})
export class AssignCrewDialogComponent implements OnInit {
  private crewControl: FormControl;

  private isLoading = false;

  private crews: Crew[] = [];

  constructor(
    private dialogRef: MatDialogRef<AssignCrewDialogComponent>,
    private httpClient: XpandHttpClient
  ) {}

  ngOnInit() {
    this.crewControl = new FormControl();

    this.crewControl.valueChanges
      .pipe(
        filter(v => typeof v === "string"),
        debounceTime(300),
        tap(() => (this.isLoading = true)),
        switchMap(value => {
          let endpoint = "/crews";
          if (value.trim()) {
            endpoint += `?q=${value}`;
          }

          return this.httpClient.get<Crew[]>(endpoint).pipe(
            tap(crews => (this.crews = crews)),
            catchError(() => {
              this.isLoading = false;
              return of<Crew[]>([]);
            }),
            finalize(() => (this.isLoading = false))
          );
        })
      )
      .subscribe();
  }

  private handleCancel(): void {
    this.dialogRef.close();
  }

  private displayValue = (crew: Crew) => {
    if (!crew) {
      return "";
    }

    const robts = this.robotsDisplayValue(crew.robots);
    return `${crew.captain}(${robts})`;
  };

  private robotsDisplayValue(robots: { name: string }[]) {
    return robots.map(r => r.name).join(", ");
  }
}

interface Crew {
  captain: string;

  robots: { name: string }[];
}
