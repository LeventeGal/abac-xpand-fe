import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignCrewDialogComponent } from './assign-crew-dialog.component';

describe('AssignCrewDialogComponent', () => {
  let component: AssignCrewDialogComponent;
  let fixture: ComponentFixture<AssignCrewDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignCrewDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignCrewDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
